<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'extensions_php_pas_fonctions' => 'Il n\'y a pas de fonctions pour cette extension PHP. Ou est-ce un Objet&nbsp;?',

	// I
	'info_1_extension_php' => 'Une extension PHP',
	'info_nb_extensions_php' => '@nb@ extensions PHP',

	// T
	'titre_lister_extensions_php' => 'Les extensions PHP chargées',
	'titre_page' => 'La liste des extensions PHP chargées',

);
