<?php

/**
 * Plugin Lister les extensions de PHP
 * Licence GPL
 *
 * @plugin     Lister les extensions PHP chargées
 * @copyright  2013-2021
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP\ListerExtensionsphp\Pipelines
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function lister_extensions_php_listermenu($flux) {
	$flux['data']['lister_extensions_php'] = array(
		'titre' => _T('lister_extensions_php:titre_lister_extensions_php'),
		'icone' => 'prive/themes/spip/images/lister_extensions_php-xx.svg',
	);

	return $flux;
}
